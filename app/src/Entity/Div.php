<?php

namespace App\Entity;

use App\Repository\DivRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\OperationInterface;


class Div implements OperationInterface
{

    public function calcul($firstOperande, $secondOperande)
    {
        if($secondOperande == 0){
            $result = 'Division by zero impossible!';
        }else{
            $result = $firstOperande / $secondOperande;
        }
        return $result;
    }
}

