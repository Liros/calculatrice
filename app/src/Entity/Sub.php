<?php

namespace App\Entity;

use App\Repository\SubRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\OperationInterface;


class Sub implements OperationInterface
{

    public function calcul($firstOperande, $secondOperande)
    {
        return $firstOperande - $secondOperande;
    }
}
