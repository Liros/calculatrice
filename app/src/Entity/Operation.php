<?php

namespace App\Entity;

use App\Repository\OperationRepository;
use Doctrine\ORM\Mapping as ORM;

use App\Entity\Add;
use App\Entity\OperationInterface;
/**
 * @ORM\Entity(repositoryClass=OperationRepository::class)
 */
class Operation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $firstOperande;

    /**
     * @ORM\Column(type="float")
     */
    private $secondOperande;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $operator;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstOperande(): ?float
    {
        return $this->firstOperande;
    }

    public function setFirstOperande(float $firstOperande): self
    {
        $this->firstOperande = $firstOperande;

        return $this;
    }

    public function getSecondOperande(): ?float
    {
        return $this->secondOperande;
    }

    public function setSecondOperande(float $secondOperande): self
    {
        $this->secondOperande = $secondOperande;

        return $this;
    }

    public function getOperator(): ?string
    {
        return $this->operator;
    }

    public function setOperator(string $operator): self
    {
        $this->operator = $operator;

        return $this;
    }

    public function calc()
    {
        $operation = null;
        switch ($this->getOperator())
        {
            case "+":
                $operation = new Add();
                break;
            case "-":
                $operation = new Sub();
                break;
            case "*":
                $operation = new Mult();
                break;
            case "/":
                $operation = new Div();
                break;
            
        }

        return $operation->calcul($this->getFirstOperande(), $this->getSecondOperande());
    }
}
