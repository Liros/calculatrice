<?php

namespace App\Entity;

use App\Repository\MultRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\OperationInterface;

class Mult implements OperationInterface
{

    public function calcul($firstOperande, $secondOperande)
    {
        return $firstOperande * $secondOperande;
    }
}