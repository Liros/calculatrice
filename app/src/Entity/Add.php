<?php

namespace App\Entity;

use App\Repository\AddRepository;
use App\Entity\OperationInterface;

class Add implements OperationInterface
{

    public function calcul($firstOperande, $secondOperande)
    {
        return $firstOperande + $secondOperande;
    }
}
