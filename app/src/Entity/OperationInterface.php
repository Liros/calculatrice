<?php

namespace App\Entity;

use App\Repository\OperationInterfaceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OperationInterfaceRepository::class)
 */
interface OperationInterface
{
    public function calcul($firstOperande, $secondOperande);
}
