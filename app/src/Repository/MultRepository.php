<?php

namespace App\Repository;

use App\Entity\Mult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mult|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mult|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mult[]    findAll()
 * @method Mult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mult::class);
    }

    // /**
    //  * @return Mult[] Returns an array of Mult objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Mult
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
