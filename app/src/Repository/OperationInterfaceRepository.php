<?php

namespace App\Repository;

use App\Entity\OperationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OperationInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method OperationInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method OperationInterface[]    findAll()
 * @method OperationInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationInterfaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OperationInterface::class);
    }

    // /**
    //  * @return OperationInterface[] Returns an array of OperationInterface objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OperationInterface
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
