<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

use App\Entity\Operation;
use App\Form\OperandeType;



class OperationController extends AbstractController
{
    #[Route('/', name: 'operation')]
    public function operate(Request $request)
    {
        $fsObject = new Filesystem();
        $current_dir_path = getcwd();
        $resultsList = "";

        try {
            $new_dir_path = $current_dir_path . "/brczr";
            //echo "new dir path =". $new_dir_path;
        
            if (!$fsObject->exists($new_dir_path))
            {
                $old = umask(0);
                $fsObject->mkdir($new_dir_path, 0775);
                $fsObject->chown($new_dir_path, "www-data");
                $fsObject->chgrp($new_dir_path, "www-data");
                umask($old);
            }
        } catch (IOExceptionInterface $exception) {
            //echo "<br>Error creating directory at". $exception->getPath();
        }
        $operation = new Operation();
        $form = $this->createForm(OperandeType::class, $operation);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $operation = $form->getData();
            $result = $operation->calc();
            try {
                $new_file_path = $current_dir_path . "/brczr/results.txt";
             
                if (!$fsObject->exists($new_file_path))
                {
                    $fsObject->touch($new_file_path);
                    $fsObject->chmod($new_file_path, 0777);
                    $fsObject->appendToFile($new_file_path, "Results:\n\n$result\n");
                }else{
                    $fsObject->appendToFile($new_file_path, "$result\n");
                }
                $resultsListFile = fopen($new_file_path, 'r');
                if ($resultsListFile) {
                    //echo 'file open';
                    while (!feof($resultsListFile)) {
                        $resultsList .= "\n".fgets($resultsListFile);
                        //var_dump($resultsList);
                        //echo $resultsList;
                    }
                    //fclose($resultsListFile);
                    //echo $resultsList;
                }
            } catch (IOExceptionInterface $exception) {
                echo "<br>Error creating file at". $exception->getPath();
            }
            return $this->render('operation/results.html.twig', array(
                'form' => $form->createView(),
                'result' => $result,
                'resultsList' => $resultsList

            ));
        }else{
            return $this->render('operation/index.html.twig', [
                'form' => $form->createView(),
                'controller_name' => 'OperationController'
            ]);
        }
    }
}
