<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

use App\Entity\Operation;


class OperationTest extends TestCase
{
    public function testOperation()
    {
        $operation = new Operation();
        $operation->setFirstOperande(21);
        $operation->setSecondOperande(23);
        $operation->setOperator("+");

        $result = $operation->calc();

        $this->assertEquals(44, $result);
    }
}
