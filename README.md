# Bricozor test

Ce projet est un test d'entretetien.

L'objectif est de réaliser une application web qui propose une calculatrice avec les quatres opérations de base (+ - x /), ainsi qu'un historique des résultats, conservé dans un fichier .txt .

## Installation

Les éléments requis sont :

- composer
- PHP
- PHPunit
- NGINX
- symfony

Le projet est sous Symfony CLI version v4.26.9.
Symfony utilise Composer afin de gérer ses dépendances.
Il faut donc dans un premier temps installer composer:

```bash
curl -Ss getcomposer.org/installer | php
```
Output similar to : Composer (version x.x.x) successfully installed to: /path/...

```bash
composer require symfony/requirements-checker
composer remove symfony/requirements-checker
```

Une image Docker est disponible [ici](https://hub.docker.com/repository/docker/cliros/php-nginx).
Il est conseiller d'avoir Docker Desktop d'installé.

Équivalent CLI:

```bash
docker pull cliros/php-nginx:phpn
docker pull cliros/php-nginx:nginxp
docker-compose up -d --build
```

PHPunit (commande à effectuer en CLI du container php de Docker Desktop ):
```bash
composer require --dev phpunit/phpunit ^9
```

## Lancement

```bash
symfony server:start
```

Lancer les tests (commande à effectuer en CLI du container php de Docker Desktop ) :
```bash
./vendor/bin/phpunit tests
```
